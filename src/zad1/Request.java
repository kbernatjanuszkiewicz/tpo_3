package zad1;

import java.io.Serializable;

public class Request implements Serializable {
    String polishWord;
    String translatedWord;
    String translationLanguage;
    int port;

    public String getPolishWord() {
        return polishWord;
    }

    public void setPolishWord(String polishWord) {
        this.polishWord = polishWord;
    }

    public String getTranslationLanguage() {
        return translationLanguage;
    }

    public void setTranslationLanguage(String translationLanguage) {
        this.translationLanguage = translationLanguage;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getTranslatedWord() {
        return translatedWord;
    }

    public void setTranslatedWord(String translatedWord) {
        this.translatedWord = translatedWord;
    }

    public Request(String polishWord, String translatedWord, String translationLanguage, int port) {
        this.polishWord = polishWord;
        this.translatedWord = translatedWord;
        this.translationLanguage = translationLanguage;
        this.port = port;
    }

    @Override
    public String toString() {
        return "polishWord: " + polishWord + ", translatedWord: " + translatedWord + ", translationLanguage: " + translationLanguage + ", port: " + port;
    }
}
