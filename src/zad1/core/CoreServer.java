package zad1.core;

import zad1.Request;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class CoreServer {
    private Socket socket = null;
    private ServerSocket serverSocket = null;
    private DataInputStream dataInputStream = null;

    private Map<String, Integer> languageServersMap = new HashMap<>() {
        {
            put("EN", 5001);
            put("DE", 5002);
            put("ES", 5003);
            put("IT", 5004);
        }
    };
    public CoreServer(int port) throws ClassNotFoundException {
        try {
            serverSocket = new ServerSocket(port);
            System.out.println("Server started");
            System.out.println("Waiting for a client ...");
            ObjectInputStream objectInputStream = null;
            ObjectOutputStream objectOutputStream = null;
            while (true) {
                socket = serverSocket.accept();
                objectInputStream = new ObjectInputStream(socket.getInputStream());
                Request request = (Request) objectInputStream.readObject();
                System.out.println(request.toString());
                redirectRequestToExactServer(request, objectOutputStream);
            }
        } catch (IOException i) {
            System.out.println(i);
        }
    }

    public void redirectRequestToExactServer(Request request, ObjectOutputStream objectOutputStream) throws IOException {
        System.out.println("I've started a redirection");
        Socket socket = new Socket("127.0.0.1", languageServersMap.get(request.getTranslationLanguage()));
        System.out.println("I'm about to send req to en server");
        objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
        objectOutputStream.writeObject(request);
    }

    public static void main(String[] args) throws ClassNotFoundException {
        CoreServer coreServer = new CoreServer(5000);
    }
}
