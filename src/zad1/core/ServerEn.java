package zad1.core;

import zad1.Request;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class ServerEn {
    private Socket socket = null;
    private ServerSocket serverSocket = null;
    private DataInputStream dataInputStream = null;
    Map<String, String> dictionaryMap = new HashMap<>() {
        {
            put("niebieski", "blue");
            put("czerwony", "red");
            put("czarny", "black");
            put("żółty", "yellow");
            put("zielony", "green");
            put("biały", "white");
            put("pomarańczowy", "orange");
            put("fiolet", "purple");
            put("brązowy", "brown");
        }
    };

    public ServerEn(int port) throws ClassNotFoundException {
        try {
            serverSocket = new ServerSocket(port);
            System.out.println("Server started");
            System.out.println("Waiting for a client ...");

            while(true) {
                socket = serverSocket.accept();
                ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
                Request request = (Request) objectInputStream.readObject();
                System.out.println(request.toString());
                System.out.println("Now, I'm about to translate the request and send it back");
                request.setTranslatedWord(translate(request));

                Socket socket2 = new Socket("127.0.0.1", request.getPort());
                System.out.println("Connected");
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket2.getOutputStream());
                objectOutputStream.writeObject(request);
            }

        } catch (IOException i) {
            System.out.println(i);
        }
    }

    private String translate(Request request) {
        return dictionaryMap.getOrDefault(request.getPolishWord(), null);
    }

    public static void main(String args[]) throws ClassNotFoundException {
        ServerEn serverEn = new ServerEn(5001);
    }
}
