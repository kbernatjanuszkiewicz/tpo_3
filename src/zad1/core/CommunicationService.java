package zad1.core;

import zad1.Request;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class CommunicationService {
    private Socket socket = null;
    private ObjectOutputStream objectOutputStream = null;
    private ObjectInputStream objectInputStream = null;

    public CommunicationService(String address, int port) {
        try {
            socket = new Socket(address, port);
            System.out.println("Connected");
            objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
        } catch (UnknownHostException u) {
            System.out.println(u);
        } catch (IOException i) {
            System.out.println(i);
        }
    }

    public void makeRequest(String polishWord, String translationLanguage, int port) throws IOException {
        Request request = new Request(polishWord, null, translationLanguage, port);
        objectOutputStream.writeObject(request);
        System.out.println("A request has been made");
    }

    public static Request waitForResponse(int port) {
        Socket socket = null;
        ServerSocket serverSocket = null;
        Request request = null;

        try {
            serverSocket = new ServerSocket(port);
            System.out.println("Port has been opened");
            System.out.println("Waiting for a response ...");
            socket = serverSocket.accept();
            System.out.println("Response communication started");
            ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
            request = (Request) objectInputStream.readObject();
            System.out.println(request.toString());
            System.out.println("Closing connection");
            socket.close();
            serverSocket.close();
            objectInputStream.close();
        } catch (IOException | ClassNotFoundException i) {
            System.out.println(i);
        }
        return request;
    }
}
