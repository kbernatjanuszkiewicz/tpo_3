package zad1.gui;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainPanel extends JPanel implements ActionListener {
    private RequestButton requestButton;
    private RequestInputTextField requestInputTextField;
    private RequestPortTextField requestPortTextField;
    JLabel jLabel = new JLabel("");
    JLabel jLabelInfo = new JLabel("Pass in colour you want to translate, pick port between 6000/7000");
    private String polishWord;
    private String translationLanguage = "";

    public MainPanel() {
        String[] languages = {
                "",
                "EN",
                "DE",
                "ES",
                "IT"
        };
        JComboBox languageListComboBox = new JComboBox(languages);

        requestInputTextField = new RequestInputTextField();
        requestPortTextField = new RequestPortTextField();
        requestButton = new RequestButton(requestInputTextField, languageListComboBox, jLabel, requestPortTextField);

        Border border = BorderFactory.createLineBorder(Color.GRAY, 1);
        Border responseBorder = BorderFactory.createTitledBorder(border, "Translation");
        Border welcomeBorder = BorderFactory.createTitledBorder(border, "Welcome");
        jLabel.setBorder(responseBorder);
        jLabel.setPreferredSize(new Dimension(380, 40));

        jLabelInfo.setBorder(welcomeBorder);
        jLabelInfo.setPreferredSize(new Dimension(380, 40));

        languageListComboBox.addActionListener(this);

        setLayout(new FlowLayout());
        add(requestInputTextField);
        add(languageListComboBox);
        add(requestPortTextField);
        add(requestButton);
        add(jLabel);
        add(jLabelInfo);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        JComboBox jComboBox = (JComboBox) actionEvent.getSource();
        translationLanguage = (String) jComboBox.getSelectedItem();
        requestButton.setTranslationLanguage(translationLanguage);
    }
}
