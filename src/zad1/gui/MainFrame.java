package zad1.gui;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame {
    JPanel jPanel = new MainPanel();
    public MainFrame() {
        super("Colours translator.");
        add(BorderLayout.CENTER, jPanel);
        setPreferredSize(new Dimension(480, 240));
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
    }
}
