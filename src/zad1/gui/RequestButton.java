package zad1.gui;

import zad1.Request;
import zad1.core.CommunicationService;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import static zad1.core.CommunicationService.waitForResponse;

public class RequestButton extends JButton implements ActionListener {
    JTextField jTextField;
    JTextField jTextFieldPort;
    JComboBox jComboBox;
    JLabel jLabel;
    String translationLanguage;
    public RequestButton(JTextField jTextField, JComboBox jComboBox, JLabel jLabel, JTextField jTextFieldPort) {
        super("click");
        this.jTextField = jTextField;
        this.jTextFieldPort = jTextFieldPort;
        this.jComboBox = jComboBox;
        this.jLabel = jLabel;
        addActionListener(this);
    }

    public void setTranslationLanguage(String translationLanguage) {
        this.translationLanguage = translationLanguage;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        int portNumber;
        try {
            portNumber = Integer.parseInt(jTextFieldPort.getText());
        } catch (NumberFormatException e) {
            jLabel.setText("Error: type in correct number format");
            return;
        }
        if(jTextField.getText() == null) {
            jLabel.setText("Error: type some word to translate");
            return;
        } else if(translationLanguage == null) {
            jLabel.setText("Error: choose translation language");
            return;
        } else if(portNumber <= 6000 || portNumber >= 7000) {
            jLabel.setText("Error: pick port between 6000 and 7000");
            return;
        }
        System.out.println(jTextField.getText());
        System.out.println(translationLanguage);
        CommunicationService communicationService = new CommunicationService("127.0.0.1", 5000);
        try {
            communicationService.makeRequest(jTextField.getText(), translationLanguage, portNumber);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Request request = waitForResponse(portNumber);
        if(request.getTranslatedWord() == null) {
            jLabel.setText("Error: there is no such word in dictionary");
            return;
        }
        jLabel.setText(request.getTranslatedWord());
    }
}
